#import "lib.typ": *
#show: rules

== Graph Algorithmen
#notation[
  - *$d[v] =$* aktuelle Distanz von $s$ nach $v quad$\
    *Invariante*: $d[v] >= mu(v)$
  - parent[v]: Vorgänger von $v$
  - $mu(v)$: Kürzester Weg zu v
]
#definition("relaxieren", code[
```
RELAX(u, v):
  newlength := d[u] + c(u, v)
  if newlength < d[v]:
    d[v] := newlength
    parent[v] := u
```
])

#theorem("#decreaseKey-Operationen", "WS22|2,4")[
  $EE["#decreaseKey-Operationen"] = O(n log m/n)$
]

=== Bucket Queue
Unsere Datenelemente sind Diskret mit $C$ Buckets ($mod (C + 1)$). Buckets speichern linked lists.
#proposition("Bucket Queue")[
// TODO: Maximum C das grade gespeichert ist constraint
  Das Minimum in der Queue steigt monoton im Verlauf der Daten.\
  Aka. wenn ein Element hinzugefügt wird ist es immer größer, als das Minimum.
]
Die Bucket Queue enthält $C$ Buckets.
Der "Anker" der Queue sei $d^*$. 
Jeder Bucket $b$ speichert eine Liste an Elementen, deren Key $k$
$k mod (C + 1)$ ist.
#proposition("Operationen")[
/ insert: Add to the respective bucket
/ decreaseKey: Move to the respective bucket
/ extractMin: If $d^*$-bucket is empty, move the $d^*$ pointer forward to the next non-empty bucket 
]
#theorem("Runtime")[
/ insert: O(1)
/ decreaseKey: O(1)
/ extractMin: O(C)
]

=== Radix Heap
Sei $C$ die maximale Differenz zwischen einem kleinsten und einem größten Wert, die irgendwann gleichzeitig in der Queue gespeichert werden.
Wir benötigen (rund) $log C$ Buckets. 

Wie bei der Bucket Queue enthält der (hier) -1-te Bucket die Keys gleich dem 'Minimum-Proxy' $d^*$.
Der $i$-te Bucket enthält die keys im Bereich $ d^* + [2^i, 2^(i+1)$).\
Bildlich: Der $i$-te Bucket enthält alle Keys, die sich in dem $i$-ten Bit unterscheiden.

#algo(title: "deleteMin")[
  if B[-1] = $emptyset$ #i\
    i := $min { j in 0..K : B[j] != emptyset}$\
    move min B[i] to B[-1] and to $d^*$\
    for $e in B[i]$ do #i\
      move e to $B[min("msd"(d^*, d[e]), K)#d#d$\
  return B[-1].pop()
]
#theorem("Radix Heap Laufzeit")[
  `deleteMin` hat Laufzeit amortisiert $O(log n)$
]
#proof[
  Jedes Element wird im Worst-Case genau einmal in jeden Bucket verschoben. -> Insgesamt $O(n log C)$.
]

=== Theorem für Dijkstra Laufzeit
#theorem("Dijkstra Worst Case")[
  $T_"Disjkstra" = O(m dot T_"decreaseKey" (n) + n dot (T_"deleteMin" (n) + T_"insert" (n)))$
]
#theorem("Dijkstra Average Case")[
  $EE[T_"DijkstraBHeap"] = O(m + n log m/n log n)$
]

=== Bidirektionale Suche
Problem: Kürzester (s, t)-Pfad
1. Mach eine Dijkstra Suche an s und t und advance beide jeweils abwechselnd
2. Maintaine ein Running min des kürzesten Pfades, der bisher gefunden wurd ($min_v {d_s [v] + d_t [v]}$)
3. Abbruchkriterium: Sobald ein Knoten in beiden Richtungen gescanned wurde
#math.arrow Der kürzeste Pfad kann nun aus dem running min ausgelesen werden

=== All-Pairs-Shorted-Paths with negative edges (=> Knotenpotentiale)
#theorem("Knotenpotentiale")[
  Sei $c: E -> RR$ eine Kostenfunktion. Sei $p: V -> RR$ eine _Potentialfunktion_.\
  Sei $overline(c) : E -> RR$ mit $overline(c)(a b) = p(a) + c(a b) - p(b)$.\
  Dann gilt für alle $s, v$-Pfade $p, q$: 
  $
    c(p) <= c(q) quad <==> quad overline(c)(p) <= overline(c)(q)
  $
]
+ Füge einen Knoten s hinzu, der mit jedem Knoten über eine 0-Kante verbunden ist
+ Berechne für jeden Knoten $v$ den kürzesten $(s, v)$-Pfad $v_"shortest"$. Definiere $p(v) = c(v_"shortest")$.
+ Nun können wir für jeden Knoten als Start Dijkstra mit Potentialen ausühren. 

=== A\*
#definition("Potential Function f (Heuristik)")[
+ $c(e) + f(v) >= f(u) quad forall_(e=u v)$
+ $f(v) <= mu(v, t) quad forall_(v in V)$
+ f(t) = 0
]
#definition("A*")[
Führe Dijkstra aus auf Graph mit Kostenfunktion $overline(c)(u b) = c(u b) + f(v) - f(u)$
]

