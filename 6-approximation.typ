#import "lib.typ" as lib: *
#show: lib.rules

= Approximationsalgorithmen
#definition[(F)PTAS][
*Minimierungsproblem:*
- Algorithmus $cal(A)$ ist ein *PTAS*, wenn $cal(A)_epsilon (x) <= (1 + epsilon) "opt"(x)$ _und_ $
T_cal(A) (n) in O(f(1/epsilon)"poly"(n))
$
- Algorithmus $cal(A)$ ist ein #text(green)[F]*PTAS*, wenn $cal(A)_epsilon (x) <= (1 + epsilon) "opt"(x)$ _und_ $
T_cal(A) (n) in O("poly"(n/epsilon)
$



Bei einem Maximierungsproblem ist die Schranke mit Approximationsfaktor umgekehrt
]
#theorem("Approximation factor for List Scheduling")[
  List scheduling greedily places the jobs on the queue with most to spare.\
  List Scheduling hat Approximationsfaktor $2 - 1/m$
]
#theorem("Nichtapproximierbarkeit von TSP")[
  Denn sonst könnte man `HamiltonCycle` polynomiell lösen.
]
#remark("Mit Dreiecksungleichung schon")[
  Mit spanning tree Konstruktion Approximationsfaktor 2.
]
#definition("Relaxation")[
  hier: ein TSP-Pfad ist ein Spezialfall eines Spannbaums.
]
#remark[Approximationsfaktor][
  Je nachdem, ob das Problem ein Minimierungs- oder Maximierungsproblem ist, ist der 
  Approximationsfaktor $1+epsilon$ oder $1 - epsilon$
]
