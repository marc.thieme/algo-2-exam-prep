#import "lib.typ" as lib: *
#show: lib.rules

= External Algorithms
#definition[Sekundärspeichermodell][
Parametrisierung über $M$ und $B$:
/ $M$: Schneller Primärspeicher der Größe $M$
/ $B$: Blockgröße bei Übertragungen in den Sekundärspeicher
]

#proposition[Externer Stapel][
(2) interne Puffer als cache
/ push: Falls alle Puffer voll, evicte einen Puffer und schreibe ihn in Datei
/ pop: Falls alle Puffer leer, lade letzten Puffer aus Datei
]
#theorem[Externer Stapel Analyse][
  Amortisiert $O(1/B)$ I/O-Operationen
]

== Externes Sortieren
#proposition[external Multiway Merge($a_1, ..., a_k$)][
  Merge sorted files $a_1, ..., a_k$. Buffer each file. Maintain a priority queue which yields the file with the lowest head.
]
#theorem[Komplexity][
  $
    O(2 n/B (1 + log n/M))
  $
]
#proposition[External Priority Queue][
First $m$ elements are buffered in heap in internal memory. Rest is stored in files of size $m$ which are accessed using $k$-way-merge.
/ Insert: Füge Element in buffer heap ein. Bei Overflow, sortiere das den buffer heap und, speichere ihn ab und füge ihn zum multiway merge hinzu
/ DeleteMin: Min aus buffer heap oder aus multiway merge
]
#theorem[Ext. Prio. Queue Komplexität][
/ deleteMin: Amortisiert $O(1 + log k)$
/ insert: Amortisiert $O(log m)$
]
