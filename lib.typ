#import "@preview/algo:0.3.3" as algorithm: algo, i, d, comment, code
// #import "@preview/lovelace:0.2.0": *
#import "lecture-notes-template/lecture-notes.typ" as template: proof

#let (
  theorem,
  definition,
  proposition,
  notation,
  remark,
  lemma,
  corollary,
  _algorithm,
  example,
) = template.init-theorems(
  theorem: "Satz",
  definition: "Definition",
  proposition: "Vorschlag",
  notation: "Notation",
  remark: "Bemerkung",
  lemma: "Lemma",
  corollary: "Korollar",
  _algorithm: "Algorithmus",
  example: "Beispiel",
)

#let algorithm(..all-args) = {
  let (..args, body) = all-args.pos()
  _algorithm(..args, ..all-args.named(), {
    set raw(lang: "python")
    body
  })
}

#let rules(body) = {
  // show: setup-lovelace
  set text(10pt)
  body
}

// #let pseudocode = pseudocode.with(line-numbering: false)
#let code = code.with(
  line-numbers: false,
  block-align: left,
  stroke: none,
  fill: none,
  inset: 0pt,
)

#let terms-to-operations(body) = {
  v(-1.2em)
  for (term, description) in body.children.map(content.fields) [
    \ #strong(raw(term.text)): _#description _
  ]
}
#let operations(body) = {
  show terms: terms-to-operations
  body
}

#let ub1(..tasks) = "ÜB1|" + tasks.pos().map(str).join(",")
#let ub2(..tasks) = "ÜB2|" + tasks.pos().map(str).join(",")
#let ub3(..tasks) = "ÜB3|" + tasks.pos().map(str).join(",")
#let ub4(..tasks) = "ÜB4|" + tasks.pos().map(str).join(",")
#let ub5(..tasks) = "ÜB5|" + tasks.pos().map(str).join(",")
#let ub6(..tasks) = "ÜB6|" + tasks.pos().map(str).join(",")

