#import "lib.typ" as lib: *
#show: lib.rules

== Addressierbare Priority Queue
Wrapped ihre Elemente in ein Handle und kann dieses auch zurückgeben. Mit diesem handle kann die Priorität 
verringert werden und Elemente gelöscht werden.
#example[
  - Dijkstra
  - Jarnik-Prim min. Spannbaum
  - Hierarchiekonstruktion für Routenplanung
  - Graphpartitionierung
  - Disk scheduling
]
#definition("Addressierbare Priority Queue")[
/ build(elems): M := elems
/ size: $->$ |M|
/ insert(e): $M:= M union {e}$
/ min: $-> min M$
/ deleteMin: $-> min M; M:=M without {e}$
/ remove(Handle h): $-> h.e; M:= M without {e}$
/ decreaseKey(handle h, Key k): key(h) := k
/ merge(W): $M:=M union W$
]
=== Pairing Heap
Darstellung erfolgt als Wald, wobei jeder Baum ein normaler Heap ist und 
die Wurzeln in einer Liste gespeichert werden. 
Das Minimum des Baumes ist das kleinste Minimum der Heaps und für schnellen 
lookup nochmal extra gespeichert. 
Wir definieren zur Bearbeitung dieses *Waldes*:
/ cut(a, h): Schneide den Teilbaum verwuzelt an h aus und füge ihn als Baum zur Liste hinzu
/ link(H, h): min(H)>min(h) => Füge h als Kind zu H hinzu
/ union(a, b), meld(a, b): link(min(a, b), max(a, b))
/ newTree(Handle h): Add the tree rooted at h to the list

Die Folgenden Operationen des Pairing Heap sind straightforward:
/ insertItem(Handle h): newTree(h)
/ decreaseKey(Handle h, k): *cut(h)*, key(h):=k, only cut _if_ root _else_ update min
/ merge(APQ other): forest := forest $union$ other.forest

das _melding_ nach *deleteMin* wird entweder mit willkürlichen Paaren gemacht.
Das ist im Gegensatz zu Fibonacci-Heaps, wo Heaps mit gleichem Rank vereint werden.
#algo(title: "deleteMin")[
m := minPtr \
forest := forest \\ {m} \
for child h of m do #i\
  newTree(h)#d\
update minPtr
]

#theorem("Pairing Heaps Runtime")[
/ insert, merge: O(1)
/ deleteMin, remove: O(log n)
/ decreaseKey: amortisiert unklar. In Praxis sehr schnell.
]

Die Kinder eines Knoten werden als doppelt verkettete Liste gespeichert. 
Der Vorteil von Pairing Heaps ist ihre einfach Implementierung und in der Praxis trotzdem
sehr gute Performance.

=== Fibonacci Heap
#definition("Fibonacci Heap Primitiven")[
/ Rang: \#Kinder eines Knoten
/ Union-By-Rank: Union/Meld nur für gleichrangige Knoten
/ Markiere Knoten: die ein Kind verloren haben
/ Kaskadierende Schnitte: Schneider markierte Knoten (die also das 2. Kind verlieren)
]
#theorem("Fibonacci Laufzeiten")[
- deleteMin: $O(log n)$
- remove: $O(log n)$
- alle anderen Operationen: $O(1)$
]
#lemma("Degree Constraint")[
  For all nodes, no two of their children have the same \#children
]

#algo(title: "deleteMin")[
m := minPtr \
forest := forest \\ {m} \
for child h of m do #i\
  newTree(h)#d\

while $exists_(a, b in "forest") "rank"(a) == "rank"(b)$ do #comment[Union by rank] #i\
  union(a, b) #comment[Es werden immer Heaps von gleicher Tiefe vereint]#d\
update minPtr
]

#algo(title: "decreaseKey")[
  key(h) := k \
  #text(blue)[cascadingCut(h)]:\
  if h is no root then#i\
    p:= parent(h)
    unmark h
    cut(h)
    if p is marked then #i \
      cascadingCut(p)#d \
    else #i\
      mark p
]
