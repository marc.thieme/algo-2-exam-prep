#import "lib.typ": *
#show: rules

= Networks
== Basics
#theorem("Duality between flows and cuts")[
Value of an $s$-$t$ max-flow = Minimum capacity of an $s$-$t$ cut.
]
#definition("Residual Graph")[
  The residual graph $G_f$ to a flow $f$ is the graph where we subtract from each
  edge the flow on that edge in $f$ and also add the flow along the edge to the
  capacity of the reverse edge.\
   This models that, in the following we can also
  take away flow we previously placed.
  $
    G_f &= (V, E_f, c^f)\
    E_f &= V^2 without {e | c_f (e) = 0}\
    c^f (a b) &= c(a b) - f(a b) + f(b a)
  $
]
#definition("Augmenting Path")[
Find a path $p$ from $s$ to $t$ such that each edge $e$ has nonzero 
#text(green)[*residual capacity*] c_e^f
]
+ Find a path from s to t
+ Saturate the smallest edge on this path
#remark("Max Flow is saturated edges")[
When you think of max flow, think in terms of saturated edges:
  #align(center, image("res/max-flow-saturated.png", width: 70%))
]
#theorem("Agumenting Paths Max Flow")[
  A flow $f$ is maximal iff $G_f$ does not have an augmenting path.
]

#definition("Blocking Flow")[
  A flow $f_b$ is said to be #text(green, [*blocking*]) iff $
    forall_("s-t-path" p) : exists_(e in p) : f_b (e) = c(e)
  $
]

== Diniz
#algo(title: "Dinitz")[
  f := 0\
  while $exists_("s-t-path" p "in" G_f)$ do #i\
    $d = G_f.#text(green, "reverseBFS") (t)$\
    $L_f = (V, lr({(u, v) in E_f : d(v) = d(u) - 1}))$
    #comment(text(green, [Layer graph]))\
    find a #text(green, [blocking flow $f_b$]) in $L_f$\
    augment $f+= f_b$#d\
  return f
]
#definition("Labelling/Levelling function constraints")[
#let label = $cal(l)$
  - $label(u) <= label(v)+1 quad (u v in E_f)$
  - $label(s) = |V|$, $label(t) = 0$

-> This is an approximation of the BFS distance from $v$ to $t$ in $G_f$
]
*TODO:* blocking flow algorithm (Greedily search for paths and saturate them)
#lemma("d(s) monotone")[
  $d(s)$ increases monotonely per iteration of the algorithm (by at least 1).
]
#theorem("Diniz Runtime")[
  Diniz runs in $O(m n^2)$
]
== Unit Capacities
#theorem("Ford Fulkersom Unit Capacities")[
Ford Fulkersom on unit capacities runs in $O(n m)$
]
#proof[
  Ford Fulkersom increases the flow by at least one in each iteration.
  Its bounded by $O(n dot "val"("max-flow"))$
  We bound the maximum flow on unit capacities by $1 n$.
  First observe that $s$ has a maximum of $n$ incident edges.
  Since the maximum flow is smaller than the outgoing capacity at $s$, we can thereby bound
  the maximum flow by $1 n$.
]
#lemma("Blocking Flow Bound")[
  There are at most $2 sqrt(m)$ blocking flow computations
]
#theorem("Diniz Unit Capacities")[
Diniz on unit capacities runs in $O((m + n) sqrt(m))$
]
#corollary("Max. Card. Bipartite Matching")[
  Diniz algorithm for Bipartite Matching: *$O((n+m)sqrt(n))$*
]
*TODO: Matching als Flow Problem odellieren*

== Preflow Push
#definition("Pre-Flow")[
  A pre-flow $f$ is a flow where the flow conservation constraint
  is relaxed:
  $
    "excess"(v) := "inflow"(v) - "outflow"(v) >= 0
  $
  $v in V\{s, t}$ is #text(green)[active] iff $"excess"(v) > 0$
]
#algo(title: "Push", parameters: ($e = (v, w)$,$delta$))[
  assert $delta$ > 0 $and$ $"excess"(v) >= delta$\
  assert residual capacity of $e >= delta$\

  excess(v) -= $delta$\
  excess(w) += $delta$\
if e is reverse edge then f(reverse(e)) -= $delta$\
else f(e) += $delta$
]
#remark("Push operation")[
  To push an edge $u v$ means to shift excess from $u$ to $v$ through
  the edge $u v$. Thereby, we reduce the capacity (or increase the reverse)
  capacity by $delta$.
]

#algo(title: "Generic Pre Flow Push")[
  $forall_(v in N(s))$ : push(s v, c(s v))\
  d(s) := n\
  $d(dot):= 0 quad (dot in V)$\
  while $exists_(v in V without {s, t}$ : excess(v) > 0 do#i #comment("active node")\
    if $exists_(e=v w in E) : d(w) < d(v)$ then#i #comment("elegible edge") \
      choose some $delta <= min{ "excess"(v), c_e^f}$\
      push(e, $delta$)#d\
    else d(v)++
]
