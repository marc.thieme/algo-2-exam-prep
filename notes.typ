#import "lecture-notes-template/lecture-notes.typ" as template
#import "lib.typ"
#show: template.project.with("Algorithmen II", "TODO", "Marc Thieme")

= Vorlesung
#include "1-data-structures.typ"
#include "2-graph-algorithms.typ"
#include "3-networks.typ"
#include "4-randomized.typ"
#include "5-external.typ"
#include "6-approximation.typ"
#include "7-parallel.typ"
#include "10-geometric.typ"
#include "11-stringology.typ"
#include "todo-cheatsheet.typ"

= TODO
#lib.ub2("4d")
