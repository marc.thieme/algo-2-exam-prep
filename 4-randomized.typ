#import "lib.typ": *
#show: rules

= Randomized Algorithms
#definition[
/ Las Vegas: Ergebnis immer korrekt, Laufzeit als Zufallsvariable.
Zb. Quicksort, Hashing
/ Monte Carlo: Ergebnis mit bestimmmter Wahrscheinlichkeit $p$
inkorrekt. $k$-fache Wiederholung macht Fehlschlagwahrscheinlichkeit
exponentiell klein ($p^k$)
]
#example([`IsPermutation`-Check])[
- Product: $<e_1, ..., e_n>$ ist Permutation von $<e'_1, ..., e'_n>$ gdw.
$
  q(z) := product^n_(i=1)(z - "field"(e_i)) - product^n_(i-1) (z - "field"(e'_i)) = 0\
  "where" "field" : {e_i | 1<=i<=n} -> FF "injectiv"
$
Dann können wir an zufälliger Stelle auswerten:
$
  PP[q != 0 and q(x) = 0] <= n/(|FF|)
$

- Hash Sum: Let $h$ be a #text(red)[random hash function] with codomain $0..(U-1)$.
$h(S) := sum_(e in S) h(e)$
$
  PP[h(E) = h(E')] <= 1/U
$
]

*TODO: Cocoo hashing*
