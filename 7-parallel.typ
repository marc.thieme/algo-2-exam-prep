#import "lib.typ" as lib: *
#show: lib.rules

= Parallel
#definition("Paralleler Algorithmus")[
$p$ Prozessoren arbeiten parallel und kommunizieren über Nachrichtenaustausch.
/ $p in NN$: RAMs parallel
/ $T_"comm" (l) = alpha + l beta$: Kosten eines 
/ $T(I, p)$: Ausührungszeit von I mit p Prozessoren
/ Speedup $S = T_"seq" / T(p)$: Ratio zu besten bekannten sequentiellen Algorithmus
/ Work $W = p T(p)$: Essentielly the sum of execution times over all processors
/ Span $T_inf = inf_p T(p)$: Zeitspanne, wenn unendlich Prozessoren verfügbar wären 
-> Parallelisierbarkeit einer Aufgabe
/ Efficiency $E = S/p$: Wenn das nicht $in Theta(1)$ ist, dann skaliert der Algorithmus nicht gut
]

Wir verwenden kein shared-memory modell, da
- es nicht skaliert
- das Kostenmaß mit Speicherzugriffskonflikten umgehen müsste
- das Nachrichtenaustauschmodell egtl alles modellieren kann
