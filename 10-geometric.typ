#import "lib.typ" as lib: *
#show: lib.rules

= Geometrische Algorithmen
#definition[Allgemeine Lage][
  - Keine horizontalen Strecken
  - Keine Überlappungen
  - Schnittpunkte jeweils im Inneren von genau zwei Strecken
]

== Streckenschnitte
Das erste wichtige Problem ist die Berechnung aller Schnittpunkte von 
einer Menge L an Strecken.

Das Problem wird mit einer Sweepline $l$ gelöst.

#definition[Status $T$][
  $T$ ist eine "nach $x$ geordnete Folge der $l$ schneidenden Strecken"
]

#definition[Ereignis][
  - Startpunkt
  - Endpunkt
  - Schnittpunkte
]

#proposition[
+ Alle bekannten Events werden in PriorityQueue (key ist y-Koordinate) abgespeichert
+ In jeder Iteration, nehme das nächste Event for 
the line $s$ intersecting $l$ currently at $x$:
  - Wenn Event start ist:\
    _Note that $T$ is ordered_
    + Take the line in $T$ previous to and following after $x$
    + Check for intersections between those two elements and $s$
]

#remark[
  Schlüssel ist der Schnittpunkt $x$ mit der sweepline.
   Der verändert sich ständig.
]

== Konvexe Hülle
#proposition[Convex Hull][
+ Sortiere alle Punkte $(x, y)$ lexikographisch (anscheinend in $O(n)$).
+ Gehe die Punkte nacheinander durch. 
+ Für jeden Punkt, lösche solange Punkte vom Ende deiner Hull,
 bis dein Hull wieder konvex ist (also bis der neue und die letzten 2
Punkte nichtmehr eine rechtsdrehung bilden.)
]

#theorem[Convex Hull Runtime][
  $
    O(n log n)
  $
]

== 2D Bereichssuche
- 1D => Einfach Segmentbaum
#proposition[Wavelet Trees mit Strings][
/ $"select"_s(k)$: An der wievielten Stelle im String ist die $k$-te occurence von $s$
/ $"rank"_s(k)$: Wie oft kommt $s$ in den ersten $k$ Indexen des Strings vor
  1. Eine Node auf Ebene $i$ enthält einen Bit-vektor der Länge $n / 2^i$ und
  einen Pfäxixsummenvektor für diesen Bit-Vektor.
  2. Die $j$-te Stelle in diesem Bit-Vektor ist true,
   wenn das Element an der Stelle in dem linken Teilbaum des Knotens ist.
  3. Invariante: Wir betrachten einen Knoten $v$ und seinen Vater $p$. Sei $v$ das rechte Kind von $p$.
  Dann gehört jedes $i$-te Element in $v$ (also seinem Bitvektor) zu der $i$-ten 1 in $p$.
]
