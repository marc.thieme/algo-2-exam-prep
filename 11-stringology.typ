#import "lib.typ" as lib: *

= Stringology
#notation[Notation][
/ $Sigma$: Alphabet
/ $T in Sigma^*$: ist ein Text
/ $|T| = n$: Länge des Textes
]

#definition[Alphabet-Arten][
/ konstante Größe: Endliche Menge unabhängig von $n$
/ Ganzahliges Alphabet: Menge ${1, ..., sigma}$ wobei $sigma$ in eine konstante Anzahl Wörter passt
/ Geordnetes Alphabet: Zeichen können nur verglichen werden.
]

#notation[Menge An Strings][
Sei $S = {s_1, ..., s_k}$ eine Menge an Strings. Dann ist 
$
  N = sum^k_(i=1) |s_i|
$ die Gesamtlänge der Strings und $k$ die Anzahl an Strings in $S$
]

== Sortieren
#algo(title: [Multikey Radix Quicksort | MKQ], parameters: ($S={s_1, ..., s_k}$, $l in NN$))[
  if k <= 1 then #i \
    return $S$ #d \
  pick pivot p\
  return #i\
    MKQ(${s in S: s[l] < p[l]}, l$)\
    MKQ(${s in S: s[l] = p[l]}, l+1$)\
    MKQ(${s in S: s[l] > p[l]}, l$)\
]
This is an improvement on radix sort because radix sort has to walk each digit/bucket at each step
(even when the bucket is empty).
For this reason, MKQ can handle common prefixes well.

== Substring Search / Mustersuche
#algorithm[Knuth-Morrison-Pratt][```
def kmp(T[1..n], P[1..m], border[1..m+1]):
  i, j = 1
  while i <= n-m + 1:
    while j <= m and T[i + j - 1] = P[j]:
      j = j + 1
    if j > m:
      P kommt in T an Position i vor
    i = i + j - border[j] - 1
    j = max(1, border[j] + 1)


def calc-border(pattern[1..m]):
  border[1..m].
  border[1] := -1
  l = 0
  for i in 2..m:
    while l >= 0 && pattern[l + 1] != pattern[i - 1]:
      l = border[l + 1]
    l += 1
    border[i] = l
```] <kmp>
